/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 04-21-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   04-20-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public with sharing class zoomCallouts {
   
    @AuraEnabled(cacheable=true)
    public static void createMeeting(string recordId){
        HttpRequest request =  new HttpRequest(); 
 
 Http http = new Http();

 Event obj = [SELECT Subject , StartDateTime , EndDateTime , Description , IsRecurrence , RecurrenceMonthOfYear , IsRecurrence2 , Recurrence2PatternStartDate , Recurrence2PatternTimeZone FROM Event where Id=:recordId ];
  request.setMethod('POST');
  request.setEndpoint('callout:Zoom_Integration/users/dCpvpUBvTeSg4TL8GCXpLg/meetings');
  request.setBody(JSON.serialize(organiseCreateMeetingData(obj)));
request.setHeader('Content-Type','application/json');
System.debug('callout');
System.debug(request);
 HTTPResponse response = http.send(request);
system.debug(request);
system.debug( response.getBody());
Map<String,Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
System.debug(results.get('join_url'));
System.debug(results.get('start_url'));
 obj.Location = (String)results.get('start_url');
 update obj;
        
    }

    private static Map<String,Object> organiseCreateMeetingData(Event obj){
       
        Map<String,Object> meetingData = new Map<String,Object>();
        // system.debug(obj.EndDateTime-obj.StartDateTime);
        Long start =  obj.StartDateTime.getTime();
        Long endDate = obj.EndDateTime.getTime();
         Long duration =  (endDate - start)/60000;
        // Long mins = duration / 60000;
        meetingData.put('topic',obj.Subject);
        meetingData.put('type' , 2);
        meetingData.put( 'start_time' , obj.StartDateTime);
        // meetingData.put('agenda' , obj.Description); 
        meetingData.put('duration' , duration);
        System.debug(meetingData);
        return meetingData;
    }
    
}